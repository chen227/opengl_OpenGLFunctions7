﻿#ifndef TRIANGLEWINDOW_H
#define TRIANGLEWINDOW_H

#include "openglwindow.h"

#include <QtGui/QGuiApplication>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QScreen>
#include <QtCore/qmath.h>

#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QOpenGLExtraFunctions>
#include <QGLWidget>
#include <qmath.h>

class TriangleWindow: public Openglwindow
{
public:
    TriangleWindow();
    ~TriangleWindow();

    void initialize() override;
    void render() override;

private:
    //位置
    GLuint m_posAttr;
    //颜色
    GLuint m_colAttr;
    //矩阵
    GLuint m_projection;
    GLuint m_view;
    GLuint m_model;
    //环境光
    GLuint m_ambientColAttr;
    //点光源
    GLuint m_lightColAttr;
    GLuint m_lightPosAttr;
    GLuint m_aNormal;
    //视点
    GLuint m_viewPosAttr;
    //纹理
    GLuint m_texcoordLocation;
    //着色器程序
    QOpenGLShaderProgram *m_program;
    int m_frame;

    //顶点缓冲对象
    GLuint VBO;
    //顶点数组对象
    GLuint VAO;
    //索引缓冲对象
    GLuint EBO;

    //纹理对象
    QOpenGLTexture *m_texture;
    GLuint textureVBO;
    //纹理对象 数组
    QVector<QOpenGLTexture *> m_vTexture;
};

#endif // TRIANGLEWINDOW_H
